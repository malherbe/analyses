<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS6">
				<head type="main">DÉSILLUSION — ESPOIR</head>
				<lg n="1">
					<l n="1" num="1.1">Oui, parfois je me prends à croire</l>
					<l n="2" num="1.2">Que c'est un rêve !… O mon pays !…</l>
					<l n="3" num="1.3">Ma France !… Couronne de gloire !…</l>
					<l n="4" num="1.4">Tes départements envahis,</l>
					<l n="5" num="1.5">Envahis après une suite</l>
					<l n="6" num="1.6">D'échecs sans un retour vainqueur !</l>
					<l n="7" num="1.7">En deux mois, où, de fuite en fuite,</l>
					<l n="8" num="1.8">Te voilà, refoulée au cœur !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Qui pourra dans le monde y croire,</l>
					<l n="10" num="2.2">Même parmi tes ennemis ?…</l>
					<l n="11" num="2.3">On connaît ta brillante histoire,</l>
					<l n="12" num="2.4">Vaste terrain, fécond semis</l>
					<l n="13" num="2.5">Des faits que choisit la victoire</l>
					<l n="14" num="2.6">Pour mettre aux arcs triomphateurs.</l>
					<l n="15" num="2.7">O France ! qui donc pourrait croire</l>
					<l n="16" num="2.8">A ces incroyables erreurs ?…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Eh bien ! mon rêve se dissipe,</l>
					<l n="18" num="3.2">Et j'y crois ; car autre jamais</l>
					<l n="19" num="3.3">Ne fut la guerre en son principe,</l>
					<l n="20" num="3.4">Quand elle vint chez nous, Français,</l>
					<l n="21" num="3.5">Même aux vieux jours de notre Gaule…</l>
					<l n="22" num="3.6">Romains, Goths, Huns, Normands, Anglais,</l>
					<l n="23" num="3.7">Prussiens… vinrent à tour de rôle,</l>
					<l n="24" num="3.8">Vainqueurs d'abord, et puis défaits.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">C'est que sur ton sol, ô ma France,</l>
					<l n="26" num="4.2">L'homme est beaucoup comme ton ciel,</l>
					<l n="27" num="4.3">Gai, bleu, charmant, tout d'espérance,</l>
					<l n="28" num="4.4">Sans rien de noir. Pétrir le fiel,</l>
					<l n="29" num="4.5">Mûrir la guerre, aiguiser l'arme,</l>
					<l n="30" num="4.6">Surveiller ou craindre un voisin…</l>
					<l n="31" num="4.7">Jamais ! Et vienne un jour d'alarme :</l>
					<l n="32" num="4.8">« Eh bien ? » dit-il au noir destin.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Et puis ce jour venu, jour sombre,</l>
					<l n="34" num="5.2">D'où l'ennemi, comme d'un bois,</l>
					<l n="35" num="5.3">Sort, assassin caché dans l'ombre,</l>
					<l n="36" num="5.4">Le vaillant cœur, il fonce !… O lois</l>
					<l n="37" num="5.5">Implacables de la puissance !</l>
					<l n="38" num="5.6">Il tombe. Mais il s'aperçoit</l>
					<l n="39" num="5.7">Du péril de son imprudence…</l>
					<l n="40" num="5.8">Debout, armé, le voici, droit !…</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Ah ! fuis, fuis bien vite, Vandale,</l>
					<l n="42" num="6.2">Toi qui déjà serrais son cou.</l>
					<l n="43" num="6.3">L'heure pourrait t'être fatale.</l>
					<l n="44" num="6.4">Il marche, il court, et si le coup</l>
					<l n="45" num="6.5">Qu'il te porte touche, ô vipère !</l>
					<l n="46" num="6.6">Ah ! c'est fini de toi ; jamais,</l>
					<l n="47" num="6.7">Jamais ton immonde repaire</l>
					<l n="48" num="6.8">Ne te verra rentrer en paix…</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1">Et mon pays de sa souillure</l>
					<l n="50" num="7.2">Se lavera devant nos yeux,</l>
					<l n="51" num="7.3">Des pieds jusques à la ceinture,</l>
					<l n="52" num="7.4">Et de la ceinture aux cheveux.</l>
					<l n="53" num="7.5">Et, tressaillante, notre France,</l>
					<l n="54" num="7.6">Dans un embrassement sans nom,</l>
					<l n="55" num="7.7">Remercira de leur vaillance</l>
					<l n="56" num="7.8">Tous ses enfants, vivants ou non !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">3 décembre 1870.</date>
					</dateline>
				</closer></div></body></text></TEI>