<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS1">
				<head type="main">SÉPARATION-ISOLEMENT</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"/>Que j'aurai donc de choses à te dire,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"/>Ma bien aimée, au jour, oh ! quel qu'il soit</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"/>Pour la patrie, ou moins mauvais ou pire,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"/>Au jour où là, dans ce petit endroit,</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"/>Notre foyer, qui, depuis deux mois vide,</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"/>S'étonne, lui qu'on voyait toujours clair,</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"/>O mon étoile ! ô mon ciel ! mon Égide !</l>
					<l n="8" num="1.8"><space unit="char" quantity="4"/>Je te verrai reparaître et parler !…</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="4"/>Je te dirai : J'étais là seul et triste,</l>
					<l n="10" num="2.2"><space unit="char" quantity="4"/>Et j'écoutais… la pendule qui bat,</l>
					<l n="11" num="2.3"><space unit="char" quantity="4"/>Et bat, et bat, sèche et morne choriste…</l>
					<l n="12" num="2.4"><space unit="char" quantity="4"/>L'huile qui brûle… un grouillement de rat…,</l>
					<l n="13" num="2.5"><space unit="char" quantity="4"/>Mon souffle… un meuble au craquement sonore…</l>
					<l n="14" num="2.6"><space unit="char" quantity="4"/>La dent d'un ver qui ronge un vieux bois, là…</l>
					<l n="15" num="2.7"><space unit="char" quantity="4"/>J'écoutais tout… et… j'écoutais encore…</l>
					<l n="16" num="2.8"><space unit="char" quantity="4"/>Car il manquait un bruit à tout cela…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="4"/>Je te dirai : J'étais là… seul, si triste !…</l>
					<l n="18" num="3.2"><space unit="char" quantity="4"/>Je regardais… Portes, meubles et murs,</l>
					<l n="19" num="3.3"><space unit="char" quantity="4"/>Plus que le mien, jamais œil d'archiviste</l>
					<l n="20" num="3.4"><space unit="char" quantity="4"/>N'avait pesé sur eux… Muets, obscurs,</l>
					<l n="21" num="3.5"><space unit="char" quantity="4"/>Ils avaient l'air bêtes à ne pas croire…</l>
					<l n="22" num="3.6"><space unit="char" quantity="4"/>Et toujours plus je cherchais çà et là…</l>
					<l n="23" num="3.7"><space unit="char" quantity="4"/>Et mon regard plongeait dans l'ombre noire…,</l>
					<l n="24" num="3.8"><space unit="char" quantity="4"/>Car il manquait son âme à tout cela.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="4"/>Et puis mon cœur, du fond de son alcôve,</l>
					<l n="26" num="4.2"><space unit="char" quantity="4"/>Effaré, blême, arrivait à son tour</l>
					<l n="27" num="4.3"><space unit="char" quantity="4"/>A sa fenêtre, ainsi qu'un oiseau fauve,</l>
					<l n="28" num="4.4"><space unit="char" quantity="4"/>Qui scrute l'ombre, et, dans son noir séjour</l>
					<l n="29" num="4.5"><space unit="char" quantity="4"/>N'entendant rien, jette à l'espace louche</l>
					<l n="30" num="4.6"><space unit="char" quantity="4"/>Un cri plaintif dont geint l'écho du lieu.</l>
					<l n="31" num="4.7"><space unit="char" quantity="4"/>Il venait mettre un soupir à ma bouche :</l>
					<l n="32" num="4.8"><space unit="char" quantity="4"/>« O ma Marie, où donc es-tu ? Mon Dieu ! »</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="4"/>Quand reviendront nos douces causeries</l>
					<l n="34" num="5.2"><space unit="char" quantity="12"/>Du soir ? Babil d'enfant</l>
					<l n="35" num="5.3"><space unit="char" quantity="16"/>Que j'aime tant,</l>
					<l n="36" num="5.4"><space unit="char" quantity="8"/>Et que mes oreilles ravies</l>
					<l n="37" num="5.5"><space unit="char" quantity="8"/>Voudraient ouïr et jours et nuits,…</l>
					<l n="38" num="5.6"><space unit="char" quantity="8"/>Mais surtout du soir à l'aurore !…</l>
					<l n="39" num="5.7"><space unit="char" quantity="8"/>Je m'endors en disant : « Et puis… »</l>
					<l n="40" num="5.8"><space unit="char" quantity="8"/>Je m'éveille en disant : « Encore. »</l>
					<l n="41" num="5.9"><space unit="char" quantity="8"/>Cause, cause, oiseau du taillis…</l>
					<l n="42" num="5.10"><space unit="char" quantity="12"/>De ton plus beau ramage,</l>
					<l n="43" num="5.11">Des sons les plus discrets, de ton bec rejaillis,</l>
					<l n="44" num="5.12"><space unit="char" quantity="10"/>Remplis l'ombre du bocage.</l>
					<l n="45" num="5.13"><space unit="char" quantity="4"/>Ah ! je t'écoute avec tant de plaisir !</l>
					<l n="46" num="5.14"><space unit="char" quantity="4"/>Ne finis pas. — Et toi, ma voix ailée,</l>
					<l n="47" num="5.15"><space unit="char" quantity="4"/>Comme l'oiseau, cause pour me ravir.</l>
					<l n="48" num="5.16"><space unit="char" quantity="4"/>Ta voix aussi charmerait la vallée !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">22 novembre, minuit.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>