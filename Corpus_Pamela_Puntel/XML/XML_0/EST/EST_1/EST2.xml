<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="EST">
					<name>
						<forename>Hector</forename>
						<surname>L'ESTRAZ</surname>
					</name>
					<date from="1848" to="1936">1848-1936</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>112 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">EST_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>HECTOR L'ESTRAZ</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb312226608</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES (D'APRÈS LES DESSINS D'ÉMILE BAYARD)</title>
								<author>HECTOR L'ESTRAZ</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE ARTISTIQUE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="EST2">
				<head type="main">1871</head>
				<lg n="1">
					<l n="1" num="1.1">Dans les guérets féconds où les futures gerbes</l>
					<l n="2" num="1.2">Offraient leur espérance en verdoyants épis,</l>
					<l n="3" num="1.3">Dans les prés où les fleurs vermeilles dans les herbes</l>
					<l n="4" num="1.4">Faisaient aux amoureux de splendides tapis,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dans les bosquets ombreux emplis de frais murmures,</l>
					<l n="6" num="2.2">Dans les sentiers secrets où l'on va deux à deux,</l>
					<l n="7" num="2.3">Dans ces riants massifs dont les vertes ramures</l>
					<l n="8" num="2.4">Ouïrent tant de mots d'amour et tant d'aveux,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Dans ces vallons aimés, pleins de joie et de vie,</l>
					<l n="10" num="3.2">Où la fauvette avait son nid dans les buissons,</l>
					<l n="11" num="3.3">Où l'on sentait son âme enivrée et ravie</l>
					<l n="12" num="3.4">Des rayons du printemps, du rire et des chansons,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tout est ruine et deuil !— L'ouragan des batailles</l>
					<l n="14" num="4.2">A balayé les fleurs dans ses noirs tourbillons</l>
					<l n="15" num="4.3">Les arbres sont hachés de profondes entailles,</l>
					<l n="16" num="4.4">La plaine a sur ses flancs de lugubres sillons.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">La colère de l'homme implacable et fatale</l>
					<l n="18" num="5.2">Ici s'est déchaînée en tempête de feu,</l>
					<l n="19" num="5.3">Dans son sauvage effet mille fois plus brutale</l>
					<l n="20" num="5.4">Que la foudre éclatante entre les mains de Dieu.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">La guerre a passé là. — Tout est mort ! — Il ne reste</l>
					<l n="22" num="6.2">Que des débris ! — Où sont les espoirs décevants ?…</l>
					<l n="23" num="6.3">La guerre a passé là ! couchant d'un coup funeste</l>
					<l n="24" num="6.4">Sa sanglante moisson d'hommes, épis vivants.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">— Et dans la plaine, va, pareille à quelque aïeule</l>
					<l n="26" num="7.2">Évoquant du passé les bonheurs fugitifs,</l>
					<l n="27" num="7.3">D'un pas désespéré, la pauvre femme, seule,</l>
					<l n="28" num="7.4">Seule avec les bébés effarés et pensifs.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">D'un œil sombre cherchant l'absent dont son Cœur rêve,</l>
					<l n="30" num="8.2">Elle s'en va traînant et sa vie et son deuil.</l>
					<l n="31" num="8.3">Pour elle, le soleil qui dans les cieux se lève,</l>
					<l n="32" num="8.4">C'est la lampe de mort brûlant près d'un cercueil.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Elle soupire : « Hélas, déchirement des âmes !</l>
					<l n="34" num="9.2">» Ces lieux riants et doux sont désolés, — oh ! voi,</l>
					<l n="35" num="9.3">» Dans ces bois, dans ces prés jadis nous nous aimames,</l>
					<l n="36" num="9.4">» O mon cher endormi, vois ! j'y reviens sans toi.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">» Pourquoi n'es-tu pas là, toi que toujours je pleure ;</l>
					<l n="38" num="10.2">» Pourquoi donc nous quitter, toi que j'attends toujours,</l>
					<l n="39" num="10.3">» Pourquoi t'en être allé si longtemps avant l'heure,</l>
					<l n="40" num="10.4">» Pourquoi donc en leur fleur briser tous nos amours ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">» Ne t'en souvient-il plus que j'étais ton idole,</l>
					<l n="42" num="11.2">» Et toi mon cher soutien ? — Pourquoi n'es-tu plus là ?</l>
					<l n="43" num="11.3">» Oh ! dis-moi, — que mon cœur avec mon âme y vole,</l>
					<l n="44" num="11.4">» Dis-le moi le pays où la mort t'exila !… »</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">— Puis, morne, contemplant les bébés qu'elle adore :</l>
					<l n="46" num="12.2">« Souvenirs les plus chers de l'amour effacé,</l>
					<l n="47" num="12.3">» Pauvres anges, croissez ! vous sourirez encore</l>
					<l n="48" num="12.4">» Vous, vivez d'avenir ! — moi je vis du passé.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">» Vos yeux où mes baisers vont essuyer vos larmes</l>
					<l n="50" num="13.2">» Auront bientôt brillé, bientôt auront souri,</l>
					<l n="51" num="13.3">» La vie aura pour vous son bonheur et ses charmes,</l>
					<l n="52" num="13.4">» Mais pour moi le bonheur en sa source est tari.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">» Vos yeux pourront encor, heureux, pleins de lumière,</l>
					<l n="54" num="14.2">» Se lever vers le ciel ; — moi, je n'ai qu'à gémir</l>
					<l n="55" num="14.3">» En contemplant la terre, espérance dernière</l>
					<l n="56" num="14.4">» Où près du trépassé je voudrais m'endormir. »</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">— Et dans la plaine, va, pareille à quelque aïeule</l>
					<l n="58" num="15.2">Évoquant du passé les bonheurs fugitifs</l>
					<l n="59" num="15.3">D'un pas désespéré, la pauvre femme, seule,</l>
					<l n="60" num="15.4">Seule avec les bébés effarés et pensifs.</l>
				</lg>
			</div></body></text></TEI>