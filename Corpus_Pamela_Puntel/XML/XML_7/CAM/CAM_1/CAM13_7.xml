<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM13" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
				<head type="main">L’HIRONDELLE</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w>-<w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="1.4">f<seg phoneme="ɥi" type="vs" value="1" rule="461" place="4">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.5" punct="pi:8">h<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>s</w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="5">e</seg>r</w> <w n="2.5">t<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="2.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="3.3">ci<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="3.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ls</w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="5">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="3.6" punct="pe:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="pe">in</seg>s</rhyme></w> !</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="4.3">P<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>t</w> <w n="4.5" punct="pe:8">b<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="1">è</seg>s</w> <w n="5.2">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rts</w> <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">où</seg></w> <w n="5.4">fl<seg phoneme="ø" type="vs" value="1" rule="404" place="4">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t</w> <w n="5.5">l</w>’<w n="5.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>sph<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>d<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">C<seg phoneme="o" type="vs" value="1" rule="414" place="1">ô</seg>t<seg phoneme="o" type="vs" value="1" rule="314" place="2">eau</seg>x</w> <w n="6.2"><seg phoneme="u" type="vs" value="1" rule="425" place="3">où</seg></w> <w n="6.3">m<seg phoneme="y" type="vs" value="1" rule="444" place="4">û</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="6.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="7">o</seg>s</w> <w n="6.5" punct="vg:8">v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" punct="vg">in</seg>s</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>s</w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="7.5">tr<seg phoneme="o" type="vs" value="1" rule="432" place="7">o</seg>p</w> <w n="7.6" punct="vg:8">v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="8" punct="vg">ain</seg>s</rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="8.5">t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6">d</w>’<w n="8.7" punct="pt:8"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="9.2" punct="pe:2">d<seg phoneme="œ" type="vs" value="1" rule="405" place="2" punct="pe">eu</seg>il</w> ! <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="414" place="3">ô</seg></w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="320" place="5">y</seg>s</w> <w n="9.5" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></rhyme></w> !</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">N<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="11.3">t</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="442" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.5">t<seg phoneme="wa" type="vs" value="1" rule="422" place="7">oi</seg></w>-<w n="11.6" punct="pt:8">m<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="8" met="8"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="12.3">dr<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>p<seg phoneme="o" type="vs" value="1" rule="314" place="4">eau</seg>x</w> <w n="12.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>cr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="12.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>c<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
					<l n="13" num="4.2" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="13.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="13.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="424" place="4" punct="vg">ou</seg>r</w>, <w n="13.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.6">d</w>’<w n="13.7"><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>r</rhyme></w></l>
					<l n="14" num="4.3" lm="8" met="8"><w n="14.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>t</w>, <w n="14.3"><seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="314" place="6">eau</seg></w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.5">j</w>’<w n="14.6" punct="pt:8"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>