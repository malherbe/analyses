<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">POÉSIES NATIONALES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAM">
					<name>
						<forename>Aimé</forename>
						<surname>CAMP</surname>
					</name>
					<date from="1812" to="1899">1812-1899</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1293 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CAM_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES NATIONALES</title>
						<author>AIMÉ CAMP</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb301894741</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES NATIONALES</title>
								<author>AIMÉ CAMP</author>
								<imprint>
									<pubPlace>PERPIGNAN</pubPlace>
									<publisher>FALIP-TASTU</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CAM12" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
				<head type="main">LES PLATANES</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="1.2" punct="vg:4">Pl<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>t</w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>s</w> <w n="2.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="2.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>s</w> <w n="2.5" punct="pi:8">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rc<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="pi">eau</seg>x</rhyme></w> ?</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg></w> <w n="3.2">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>ct</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>rc<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg>x</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>st<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="4.4">s</w>’<w n="4.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2" punct="vg:3">f<seg phoneme="œ" type="vs" value="1" rule="405" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="5.3">d</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="4">un</seg></w> <w n="5.5">bru<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg>t</w> <w n="5.6" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>r</w> <w n="6.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg>s</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="6.5" punct="pv:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="pv">eau</seg>x</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4">e</seg>s</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="314" place="8">eau</seg>x</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="8.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="8.4">v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="9.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="6">en</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rt</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">p<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="248" place="5">œu</seg>r</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="10.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7" punct="pt:8">m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>rd</rhyme></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">E</seg>st</w>-<w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg></w> <w n="11.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>c</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="11.6" punct="pi:8">fl<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>tr<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="8" met="8"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg></w> <w n="12.2">d<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w>-<w n="12.3" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="449" place="3" punct="vg">u</seg></w>, <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>s</w> <w n="12.5">br<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>x</w> <w n="12.7" punct="vg:8">y<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="13" num="4.2" lm="8" met="8"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="13.3">d<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">br<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">ci<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="14" num="4.3" lm="8" met="8"><w n="14.1" punct="vg:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="ø" type="vs" value="1" rule="404" place="2">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>r</w>, <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="414" place="4">ô</seg></w> <w n="14.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4" punct="pi:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>tr<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
				</lg>
			</div></body></text></TEI>