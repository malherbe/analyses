<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP23" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="10((aa))">
				<head type="number">XXIII</head>
				<head type="main">L'AMI</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>'<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="1.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.8">l</w>'<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>s</w> <w n="1.10" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="434" place="11" mp="M">o</seg>nn<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pt">u</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="distiques" rhyme="aa…">
					<l n="2" num="2.1" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>s</w> <w n="2.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="2.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="9" mp="P">u</seg>r</w> <w n="2.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="2.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11">on</seg>t</w> <w n="2.10" punct="vg:12">n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="vg">u</seg></rhyme></w>,</l>
					<l n="3" num="2.2" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="3.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="3.4">n<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>rs</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" mp="M">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="P">u</seg>r</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="3.9" punct="vg:12">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="4" num="2.3" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">É</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="M">u</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5" mp="C">un</seg></w> <w n="4.3">l<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="4.6">cl<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="4.8" punct="pt:12">l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="5" num="2.4" lm="12" met="6+6"><w n="5.1">C</w>'<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="408" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>t</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3" mp="C">un</seg></w> <w n="5.4">tr<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="5" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="7" mp="Lc">è</seg>s</w>-<w n="5.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="M/mc">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="484" place="9">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="5.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="409" place="11" mp="Lc">è</seg>s</w>-<w n="5.9" punct="vg:12">d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="6" num="2.5" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="6.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="6.3">s</w>'<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg></w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg></w> <w n="6.9" punct="pt:12">n<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pt">ou</seg>s</rhyme></w>.</l>
					<l n="7" num="2.6" lm="12" met="6+6"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="C">ou</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>s</w> <w n="7.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="7.7">pl<seg phoneme="y" type="vs" value="1" rule="449" place="9">u</seg>s</w> <w n="7.8">l</w>'<w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="10">un</seg></w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="188" place="11">e</seg>t</w> <w n="7.11">l</w>'<w n="7.12" punct="pt:12"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="317" place="12">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="8" num="2.7" lm="12" met="6+6"><w n="8.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="443" place="1" punct="pe">O</seg>h</w> ! <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="8.4">ch<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="8.7" punct="pe:12">n<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="414" place="12">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="9" num="2.8" lm="12" met="6+6"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="C">ou</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.7">pr<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="9">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.8" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="11" mp="M">ê</seg>v<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="vg">eu</seg>rs</rhyme></w>,</l>
					<l n="10" num="2.9" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">c<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="P">ou</seg>r</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="10.5">l</w>'<w n="10.6"><seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="10.8">d</w>'<w n="10.9"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.10" punct="dp:12">s<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>v<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="dp">eu</seg>rs</rhyme></w> :</l>
					<l n="11" num="2.10" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="11.2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="11.3">c</w>'<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="11.5" punct="dp:6">C<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="dp" caesura="1">om</seg>b</w> :<caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="8">en</seg></w> <w n="11.8">c</w>'<w n="11.9"><seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="11.10" punct="pt:12">Sh<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>kesp<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="" place="12">ea</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="12" num="2.11" lm="12" met="6+6"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="C">i</seg>l</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="12.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="12.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.8">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10" mp="M">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11">ai</seg>s</w> <w n="12.9" punct="vg:12">d<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="13" num="2.12" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="439" place="2" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5" mp="P">an</seg>s</w> <w n="13.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="13.6">tr<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="63" place="9">e</seg>rs</w> <w n="13.7">l</w>'<w n="13.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="443" place="10" mp="M">O</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="vg">an</seg></rhyme></w>,</l>
					<l n="14" num="2.13" lm="12" met="6+6"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="14.8" punct="pt:12">n<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>t</rhyme></w>.</l>
					<l n="15" num="2.14" lm="12" met="6+6"><w n="15.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>ls</w> <w n="15.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="189" place="3">e</seg>ts</w> <w n="15.3">d</w>'<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="15.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>s</w> <w n="15.7">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>ts</w> <w n="15.8" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>s<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="16" num="2.15" lm="12" met="6+6"><w n="16.1">C</w>'<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="16.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="2">en</seg></w> <w n="16.4" punct="vg:3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="3" punct="vg">oin</seg></w>, <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="16.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>s</w> <w n="16.8"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="C">i</seg>l</w> <w n="16.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="16.10">s<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="17" num="2.16" lm="12" met="6+6"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">c</w>'<w n="17.3"><seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="17.4">hi<seg phoneme="ɛ" type="vs" value="1" rule="63" place="4">e</seg>r</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="17.6">qu</w>'<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l</w> <w n="17.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.9">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="10">ai</seg>t</w> <w n="17.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="11" mp="M">ain</seg>s<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg></rhyme></w></l>
				</lg>
				<lg n="3" type="distiques" rhyme="aa…">
					<l n="18" num="3.1" lm="12" met="6+6"><w n="18.1">D</w>'<w n="18.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="18.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="18.4">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="456" place="6" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="18.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>l</w> <w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="18.7">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="9">o</seg>rt</w> <w n="18.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="10">oin</seg></w> <w n="18.9">d</w>'<w n="18.10" punct="dp:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>c<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="dp">i</seg></rhyme></w> :</l>
					<l n="19" num="3.2" lm="12" met="6+6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="19.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">où</seg></w> <w n="19.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="19.8">s<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="19.9" punct="ps:12">c<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
				</lg>
				<lg n="4" type="distiques" rhyme="aa…">
					<l n="20" num="4.1" lm="12" met="6+6"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" mp="C">on</seg></w> <w n="20.3" punct="pe:3">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="pe">eu</seg></w> ! <w n="20.4">qu</w>'<w n="20.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="20.6">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="20.7">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="7" mp="C">e</seg>t</w> <w n="20.8" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" punct="vg">an</seg>t</w>, <w n="20.9">p<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="P">ou</seg>r</w> <w n="20.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="20.11" punct="pe:12">pr<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 22 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>