<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">L'INVASION</title>
				<title type="sub">1870</title>
				<title type="medium">Édition électronique</title>
				<author key="DLP">
					<name>
						<forename>Albert</forename>
						<surname>DELPIT</surname>
					</name>
					<date from="1849" to="1893">1849-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d'erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1924 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">DLP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>L'INVASION 1870</title>
						<author>ALBERT DELPIT</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k54456562.r=delpit%20l%27invasion?rk=42918;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L'INVASION 1870</title>
								<author>ALBERT DELPIT</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LACHAUD</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLP8" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="10((aa))">
				<head type="number">VIII</head>
				<head type="main">DANS LA NUIT</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2" punct="vg:3">nu<seg phoneme="i" type="vs" value="1" rule="490" place="3" punct="vg">i</seg>t</w>, <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="C">i</seg>l</w> <w n="1.4">pl<seg phoneme="ø" type="vs" value="1" rule="404" place="5" mp="M">eu</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t</w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="11">ai</seg>t</w> <w n="1.9" punct="pv:12">f<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>rt</rhyme></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="2.2">P<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="2.4">d<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7" mp="C">un</seg></w> <w n="2.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.8" punct="dp:12">m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="12" punct="dp">o</seg>rt</rhyme></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1">en</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l</w>'<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="314" place="3">eau</seg></w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="P">u</seg>r</w> <w n="3.8">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="3.9">v<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="11">en</seg></w> <w n="3.11" punct="pt:12">cl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="443" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="1" punct="vg">O</seg>r</w>, <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="4.4">h<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="Fm">e</seg>s</w>-<w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="4.6">l</w>'<w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="4.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="9">oi</seg>t</w> <w n="4.9">m<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.10">ch<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="443" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="2">er</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="5.6" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>ts</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>rs</w><caesura></caesura> <w n="6.4">tr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg></w> <w n="6.6" punct="pv:12">r<seg phoneme="ɛ" type="vs" value="1" rule="338" place="10" mp="M">a</seg>y<seg phoneme="o" type="vs" value="1" rule="434" place="11" mp="M">o</seg>nn<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pv">an</seg>ts</rhyme></w> ;</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1" mp="M">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>rs</w> <w n="7.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="3" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="4">ô</seg>t</w> <w n="7.3" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>ç<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg>s</w> <w n="7.5" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="466" place="11" mp="M">i</seg>n<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="8.2">h<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg>x</w> <w n="8.6">h<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.7">d</w>'<w n="8.8" punct="dp:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="418" place="11" mp="M">o</seg>mn<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</rhyme></w> :</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="300" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.5">tr<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="9">en</seg>t</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="317" place="10" mp="C">au</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>ss<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="10.4">m<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="10.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="352" place="10" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>c<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="12">é</seg></rhyme></w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Où</seg></w> <w n="11.2">l</w>'<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="11.6">j<seg phoneme="wa" type="vs" value="1" rule="422" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="11.8">s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="11.9">v<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.10" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>rd<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="2" type="distiques" rhyme="aa…">
					<l n="12" num="2.1" lm="12" met="6+6"><w n="12.1">J</w>'<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="1" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="12.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="12.5">t<seg phoneme="o" type="vs" value="1" rule="443" place="7" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="8">er</seg></w> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="9" mp="P">an</seg>s</w> <w n="12.7">l</w>'<w n="12.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>d<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="distiques" rhyme="aa…">
					<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="13.2" punct="pe:3">m<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="492" place="3" punct="pe">y</seg>rs</w> ! <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="P">à</seg></w> <w n="13.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="346" place="10">ez</seg></w> <w n="13.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="P">ou</seg>r</w> <w n="13.8" punct="pe:12">n<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="pe">ou</seg>s</rhyme></w> !</l>
					<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1">M<seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg></w> <w n="14.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="422" place="3" punct="vg">oi</seg></w>, <w n="14.3">l</w>’<w n="14.4"><seg phoneme="œ" type="vs" value="1" rule="285" place="4">œ</seg>il</w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="317" place="5" mp="C">au</seg></w> <w n="14.6" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="14.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.9">m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>s</w> <w n="14.10"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="14.11" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="12" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="15" num="3.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg></w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="P">ou</seg>r</w> <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="15.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg></w> <w n="15.8">fr<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="339" place="10" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>t</w> <w n="15.10">l</w>'<w n="15.11" punct="ps:12">h<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					<l n="16" num="3.4" lm="12" met="6+6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="16.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="16.8" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="17" num="3.5" lm="12" met="6+6"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="17.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">in</seg>gt</w> <w n="17.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3" punct="vg">an</seg>s</w>, <w n="17.4">c<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="M">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="434" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="17.6">b<seg phoneme="o" type="vs" value="1" rule="443" place="8" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>r</w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="17.8">d</w>'<w n="17.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>sp<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="18" num="3.6" lm="12" met="6+6"><w n="18.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="1">oin</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="18.3">c<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>x</w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="18.6">v<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="18.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>vi<seg phoneme="e" type="vs" value="1" rule="346" place="10">ez</seg></w> <w n="18.9" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="12" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="19" num="3.7" lm="12" met="6+6"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="19.4" punct="vg:6">t<seg phoneme="i" type="vs" value="1" rule="492" place="5" mp="M">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="vg" caesura="1">an</seg>s</w>,<caesura></caesura> <w n="19.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>ts</w> <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="19.8" punct="vg:12">gl<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="20" num="3.8" lm="12" met="6+6"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg>t</w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="M">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg></w> <w n="20.3">j<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="4">oin</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.5"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="20.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="20.8">l</w>'<w n="20.9" punct="pe:12">h<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>st<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="419" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Paris, 9 octobre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>