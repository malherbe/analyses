<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRK28" modus="sp" lm_max="8" metProfile="8, (6)" form="suite périodique" schema="6(abbacc)">
				<head type="main">VIVE LA RÉPUBLIQUE !</head>
				<lg n="1" type="sizain" rhyme="abbacc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">BI<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">EN</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="1.4">P<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>s</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="1.7" punct="vg:8">st<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>p<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="2.3">y<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="2.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="304" place="6">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ci<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8">—<w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="3.4">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg></w> <w n="3.6" punct="tc:8">vi<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg ti">e</seg></rhyme></w>,—</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="437" place="2">o</seg>t</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>t</w> <w n="4.4">f<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.5" punct="vg:8">p<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu</w>’<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="5.3">d<seg phoneme="ɔ" type="vs" value="1" rule="418" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="5.5">r<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="5.7" punct="ps:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>pl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
					<l n="6" num="1.6" lm="6"><space unit="char" quantity="4"></space><w n="6.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="6.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="abbacc">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1" punct="pe:3">L<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="408" place="3" punct="pe">é</seg></w> ! <w n="7.2">Cr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg></w> <w n="7.3">j<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>br<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>t</rhyme></w></l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">S<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5" punct="pe:8"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rs<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">t<seg phoneme="i" type="vs" value="1" rule="492" place="4">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="6">i</seg></w> <w n="9.5" punct="pv:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>c<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="10.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="10.6" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>r<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe ti">an</seg>t</rhyme></w> !—</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1" punct="vg:2">P<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="11.5">l<seg phoneme="wa" type="vs" value="1" rule="422" place="6">oi</seg></w> <w n="11.6">s</w>’<w n="11.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ppl<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="12" num="2.6" lm="6"><space unit="char" quantity="4"></space><w n="12.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="12.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="abbacc">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>g<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="13.2">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg></w> <w n="13.4" punct="vg:8">n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>v<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">P<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="14.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="14.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="vg:8">h<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="15.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="15.4">C<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7">an</seg>s</w> <w n="15.6">t<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1">im</seg>p<seg phoneme="o" type="vs" value="1" rule="443" place="2">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg></w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>v<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="314" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">S<seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">p<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4" punct="dp:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>l<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="18" num="3.6" lm="6"><space unit="char" quantity="4"></space><w n="18.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="18.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="abbacc">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1" punct="pe:4">Fr<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="4" punct="pe">é</seg></w> ! <w n="19.2">M<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>s</w> <w n="19.3" punct="pe:8"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pe">i</seg>s</rhyme></w> !</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="1">Ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w>-<w n="20.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="vg">ou</seg>s</w>, <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>r</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t</w> <w n="20.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="20.7" punct="pe:8"><rhyme label="b" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="21" num="4.3" lm="8" met="8"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="21.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="21.7" punct="vg:8">c<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>t</w> <w n="22.5" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1">Ch<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="23.3">h<seg phoneme="ɛ" type="vs" value="1" rule="304" place="4">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="œ" type="vs" value="1" rule="285" place="6">œ</seg>il</w> <w n="23.7" punct="dp:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>bl<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="24" num="4.6" lm="6"><space unit="char" quantity="4"></space><w n="24.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="24.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="5" type="sizain" rhyme="abbacc">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="25.2"><seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.4">n<seg phoneme="o" type="vs" value="1" rule="437" place="6">o</seg>s</w> <w n="25.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="342" place="7">a</seg>ï<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
					<l n="26" num="5.2" lm="8" met="8"><w n="26.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="414" place="3">ô</seg></w> <w n="26.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="27" num="5.3" lm="8" met="8"><w n="27.1">H<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg></w> <w n="27.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="27.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.6" punct="pe:8">r<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>d<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="28" num="5.4" lm="8" met="8"><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>r</w> <w n="28.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4">en</seg>d<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rd</w> <w n="28.4">gl<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="13" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>x</rhyme></w></l>
					<l n="29" num="5.5" lm="8" met="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2" punct="vg:2">l<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>s</w>, <w n="29.3">s<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>s</w> <w n="29.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.5">f<seg phoneme="ɛ" type="vs" value="1" rule="63" place="5">e</seg>r</w> <w n="29.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="29.8" punct="dp:8">p<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="30" num="5.6" lm="6"><space unit="char" quantity="4"></space><w n="30.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="30.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="6" type="sizain" rhyme="abbacc">
					<l n="31" num="6.1" lm="8" met="8"><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="31.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="2">om</seg></w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="31.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="490" place="7">i</seg>t<rhyme label="a" id="16" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="32" num="6.2" lm="8" met="8"><w n="32.1" punct="pe:2">Pl<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe ps">e</seg></w> !… <w n="32.2">P<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>r</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.4">si<seg phoneme="ɛ" type="vs" value="1" rule="409" place="5">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.5"><seg phoneme="u" type="vs" value="1" rule="425" place="6">où</seg></w> <w n="32.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="32.7" punct="vg:8">s<rhyme label="b" id="17" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="33" num="6.3" lm="8" met="8"><w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="33.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="33.4" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="vg">em</seg>ps</w>, <w n="33.5">p<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>r</w> <w n="33.6">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>s</w> <w n="33.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="33.8" punct="vg:8">h<rhyme label="b" id="17" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="418" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="34" num="6.4" lm="8" met="8"><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="34.2">l</w>’<w n="34.3"><seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.4" punct="vg:8">P<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>st<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="16" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="35" num="6.5" lm="8" met="8">—<w n="35.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="35.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="35.3" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>rt</w>, <w n="35.4">tr<seg phoneme="o" type="vs" value="1" rule="414" place="4">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="35.6" punct="tc:8">b<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>l<rhyme label="c" id="18" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ti">e</seg></rhyme></w>—</l>
					<l n="36" num="6.6" lm="6"><space unit="char" quantity="4"></space><w n="36.1">V<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="36.3" punct="pe:6">R<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>bl<rhyme label="c" id="18" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>