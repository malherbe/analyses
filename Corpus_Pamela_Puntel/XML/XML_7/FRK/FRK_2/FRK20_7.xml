<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><head type="main_subpart">PATRIE</head><div type="poem" n="3" key="FRK20" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd eed">
							<head type="number">III</head>
							<lg n="1" rhyme="abab">
								<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2" punct="vg">on</seg>s</w>, <w n="1.2" punct="vg:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="1.6">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.8">pr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
								<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="7">un</seg></w> <w n="2.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>d</w> <w n="2.9">p<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.10" punct="pv:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="11" mp="M">ain</seg>c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pv">u</seg></rhyme></w> ;</l>
								<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>s</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="424" place="4" mp="P">ou</seg>r</w> <w n="3.4">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="317" place="9">au</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="490" place="10">i</seg></w> <w n="3.9">n<seg phoneme="u" type="vs" value="1" rule="424" place="11" mp="C">ou</seg>s</w> <w n="3.10" punct="vg:12">br<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
								<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg>x</w> <w n="4.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="2">ain</seg>s</w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="6" caesura="1">ue</seg>il</w><caesura></caesura> <w n="4.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg></w> <w n="4.9" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="449" place="10" mp="M">u</seg>rv<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>c<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pe">u</seg></rhyme></w> !</l>
							</lg>
							<lg n="2" rhyme="abab">
								<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">tr<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="306" place="6" caesura="1">a</seg>il</w><caesura></caesura> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="5.7" punct="dp:12">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
								<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="P">à</seg></w> <w n="6.4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="6.10">s</w>’<w n="6.11"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="11">e</seg>st</w> <w n="6.12" punct="pe:12">t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pe">u</seg></rhyme></w> !</l>
								<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>r</w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="7.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9" mp="M">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="7.9" punct="pe:12">j<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="422" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
								<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="C">Au</seg></w> <w n="8.2">st<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.4" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="dp in">on</seg>s</w> : « <w n="8.5">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.6">n<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="C">ou</seg>s</w> <w n="8.7">v<seg phoneme="ø" type="vs" value="1" rule="397" place="11" mp="Lp">eu</seg>x</w>-<w n="8.8" punct="pi:12">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="12" punct="pi">u</seg></rhyme></w> ?»</l>
							</lg>
							<lg n="3" rhyme="ccd">
								<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg></w>, <w n="9.2">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="9.4" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6" punct="vg" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="9">o</seg>r</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.9">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="9.10" punct="vg:12">g<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
								<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg></w> <w n="10.3">c<seg phoneme="œ" type="vs" value="1" rule="248" place="3">œu</seg>r</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.5" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="9" mp="C">e</seg>s</w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="449" place="10">u</seg>s</w> <w n="10.9">s<seg phoneme="y" type="vs" value="1" rule="449" place="11" mp="M">u</seg>p<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
								<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">t<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="11.3">ch<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="11.7">t<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="11.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.9" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="10" mp="M">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pe">a</seg>s</rhyme></w> !</l>
							</lg>
							<lg n="4" rhyme="eed">
								<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="tc:1"><seg phoneme="e" type="vs" value="1" rule="188" place="1" punct="ti">E</seg>t</w> — <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="C">ou</seg>s</w> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="12.5">l</w>’<w n="12.6" punct="vg:9"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="408" place="8" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="414" place="10">ô</seg></w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="12.9" punct="pi:12">m<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ti" mp="F">e</seg></rhyme></w> ?—</l>
								<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="M">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="3">un</seg></w> <w n="13.4">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>l</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="352" place="5" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="13.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="13.7">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rv<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="408" place="10" mp="M">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">é</seg>m<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
								<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ls</w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="408" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="14.5">t<seg phoneme="y" type="vs" value="1" rule="449" place="7" mp="C">u</seg></w> <w n="14.6">f<seg phoneme="ɛ" type="vs" value="1" rule="357" place="8" mp="M">e</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="10">a</seg>s</w> <w n="14.7">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="14.8" punct="pe:12">br<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="pe">a</seg>s</rhyme></w> !</l>
							</lg>
						</div></body></text></TEI>