<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">CHANTS DE COLÈRE</title>
				<title type="medium">Édition électronique</title>
				<author key="FRK">
					<name>
						<forename>Félix</forename>
						<surname>FRANK</surname>
					</name>
					<date from="1837" to="1895">1837-1895</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1139 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
						<author>FÉLIX FRANK</author>
					</titleStmt>
					<publicationStmt>
						<publisher>BNF</publisher>
						<pubPlace>Paris</pubPlace>
						<idno type="URI">https://catalogue.bnf.fr/ark:/12148/cb30460629p</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANTS DE COLÈRE. L’EMPIRE — L’INVASION — LES ÉPAVES</title>
								<author>FÉLIX FRANK</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LEMERRE</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉPAVES</head><div type="poem" key="FRK26" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee">
					<head type="main">L’ÉCOLE</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>L</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>ps</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.5">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="189" place="8" mp="C">e</seg>t</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.9" punct="vg:12">fr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="3">o</seg>r</w> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="424" place="5" mp="M">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="7" mp="P">an</seg>s</w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="8" mp="C">un</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="339" place="9">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.10">f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="63" place="12">e</seg>r</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="3.7">b<seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>s</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="3.10" punct="ps:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">En</seg>f<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="63" place="12" punct="ps">e</seg>r</rhyme></w>…</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="4.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>ps</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>t</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="188" place="11">e</seg>t</w> <w n="4.10" punct="pe:12">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Pu<seg phoneme="i" type="vs" value="1" rule="490" place="1" mp="Lc">i</seg>squ</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg></w> <w n="5.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="3">om</seg></w> <w n="5.4">d</w>’<w n="5.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443" place="5" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="5.6">l<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rd</w> <w n="5.7" punct="vg:9">st<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>gm<seg phoneme="a" type="vs" value="1" rule="339" place="9" punct="vg">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" mp="C">on</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>cc<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r</w> <w n="6.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="199" place="3">om</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.5" punct="vg:6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="5" mp="M">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="6.6">p<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7" punct="vg:9">P<seg phoneme="œ" type="vs" value="1" rule="406" place="9" punct="vg">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="188" place="10">e</seg>t</w> <w n="6.9">qu</w>’<w n="6.10">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="63" place="12">e</seg>r</rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="7.2">d<seg phoneme="e" type="vs" value="1" rule="408" place="2" mp="M">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="C">ou</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg></w> <w n="7.7">c<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="10">é</seg></w> <w n="7.8">s<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg></w> <w n="7.9" punct="vg:12">ch<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="63" place="12" punct="vg">e</seg>r</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="8.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>ps</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="408" place="4" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="8.7">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.9">l</w>’<w n="8.10" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="408" place="11" mp="M">É</seg>c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="3" rhyme="cdd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>l</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="9.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">em</seg>ps</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.5" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5" mp="M">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>thl<seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">in</seg>s<seg phoneme="u" type="vs" value="1" rule="424" place="10" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="10.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t</w> <w n="10.3">pu<seg phoneme="i" type="vs" value="1" rule="490" place="3" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5">i</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="424" place="6" caesura="1">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="10.7">t<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="10.8">f<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.9">qu<seg phoneme="i" type="vs" value="1" rule="490" place="11">i</seg></w> <w n="10.10">s<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="12">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="C">a</seg></w> <w n="11.3">v<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg></w> <w n="11.4">gr<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="11.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="8">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10">on</seg></w> <w n="11.9">t</w>’<w n="11.10" punct="dp:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="11" mp="M">en</seg>s<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="383" place="12">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					</lg>
					<lg n="4" rhyme="cee">
						<l n="12" num="4.1" lm="12" met="6+6">—<w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="12.2">pl<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>rs</w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="12.4">t</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="M">eu</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="12.6">s<seg phoneme="e" type="vs" value="1" rule="408" place="7" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="9">on</seg>t</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" mp="P">an</seg>s</w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="11" mp="C">e</seg>s</w> <w n="12.9" punct="vg:12">y<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="397" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="449" place="2" mp="C">u</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" mp="M">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="8">e</seg>c</w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="13.6">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.7">t<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="14.2" punct="vg:2">c<seg phoneme="œ" type="vs" value="1" rule="248" place="2" punct="vg">œu</seg>r</w>, <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3" mp="C">e</seg>s</w> <w n="14.4">br<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="14.6">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>rts</w><caesura></caesura> <w n="14.7">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="C">a</seg></w> <w n="14.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.10" punct="pe:12">b<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>t<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="306" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
					</lg>
				</div></body></text></TEI>