<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ37" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="5[abab] 1[ababbcbc]">
					<head type="number">XXXVII</head>
					<head type="main">INVITATION</head>
					<head type="sub_1">Le roi Guillaume <lb></lb> à LL. MM. les rois de Wurtemberg, <lb></lb>Bavière, etc</head>
					<lg n="1" type="regexp" rhyme="ababababababababbcbcabababab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="œ" type="vs" value="1" rule="150" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="396" place="2">eu</seg>r</w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.3" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" punct="vg">in</seg></w>, <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>s</w> <w n="1.6">pr<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg></w> <w n="2.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="367" place="5">en</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.4" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.3" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="vg:4">sp<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="317" place="5">au</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg></w> <w n="4.5" punct="pt:8">pr<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>x</rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="5.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="346" place="7">er</seg></w> <w n="5.6">d</w>'<w n="5.7" punct="pv:8"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1" punct="vg">ai</seg>s</w>, <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="a" type="vs" value="1" rule="193" place="6">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="424" place="5">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="314" place="6">eau</seg>x</w> <w n="7.5" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>d<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="8.4">Fr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>tz</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="8.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pt">é</seg></rhyme></w>.</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="9.2">n</w>'<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6">f<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg></w> <w n="9.7">d</w>'<w n="9.8"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>f<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>s</w> <w n="10.4">l</w>'<w n="10.5"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="10.7" punct="vg:8">ci<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="11.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">em</seg>ps</w> <w n="11.6">s<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>t</w> <w n="11.7" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>p<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="12.2">m<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="12.4" punct="pt:8">gr<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">Str<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>sb<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rg</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="13.3">v<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">b<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.6" punct="pv:8">fl<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="14.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="14.4">n</w>'<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="14.6" punct="vg:6">v<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg">u</seg></w>, <w n="14.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8" punct="vg:8">cr<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.2">d<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>x</w> <w n="15.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>s</w> <w n="15.4">d</w>'<w n="15.5" punct="dp:8"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></w> :</l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1">V<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="3">ez</seg></w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6" punct="pt:8">r<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="17" num="1.17" lm="8" met="8"><w n="17.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="1">en</seg></w> <w n="17.2">qu</w>'<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="17.5">v<seg phoneme="ø" type="vs" value="1" rule="247" place="4">œu</seg>x</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg></w> <w n="17.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">in</seg>gr<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t</rhyme></w></l>
						<l n="18" num="1.18" lm="8" met="8"><w n="18.1">N</w>'<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>t</w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="18.4">v<seg phoneme="u" type="vs" value="1" rule="424" place="3">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg></w> <w n="18.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rm<rhyme label="c" id="9" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="vg">er</seg></rhyme></w>,</l>
						<l n="19" num="1.19" lm="8" met="8"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">p<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>cr<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="20" num="1.20" lm="8" met="8"><w n="20.1">J</w>'<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="409" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="20.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="7">ai</seg>m<rhyme label="c" id="9" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="346" place="8" punct="pt">er</seg></rhyme></w>.</l>
						<l n="21" num="1.21" lm="8" met="8"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="21.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg></w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<rhyme label="a" id="10" gender="f" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="22" num="1.22" lm="8" met="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="22.2">r<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>n<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="22.3">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="6">e</seg>s</w> <w n="22.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>spr<rhyme label="b" id="11" gender="m" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>ts</rhyme></w>,</l>
						<l n="23" num="1.23" lm="8" met="8"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">v<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>x</w> <w n="23.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="23.5">y<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>x</w> <w n="23.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="23.8">Fr<rhyme label="a" id="10" gender="f" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="24" num="1.24" lm="8" met="8"><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">Un</seg></w> <w n="24.2">v<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">am</seg>b<seg phoneme="o" type="vs" value="1" rule="314" place="5">eau</seg></w>' <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>r<rhyme label="b" id="11" gender="m" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
						<l n="25" num="1.25" lm="8" met="8"><w n="25.1">L</w>'<w n="25.2"><seg phoneme="y" type="vs" value="1" rule="452" place="1">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rt</w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.5" punct="pv:8">t<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>l<rhyme label="a" id="12" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="26" num="1.26" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">Di<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="26.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">om</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ts</w> <w n="26.5"><seg phoneme="i" type="vs" value="1" rule="496" place="6">y</seg></w> <w n="26.6" punct="pv:8">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="7">en</seg>dr<rhyme label="b" id="13" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg></rhyme></w> ;</l>
						<l n="27" num="1.27" lm="8" met="8"><w n="27.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="27.2">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="27.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">om</seg>pl<rhyme label="a" id="12" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="28" num="1.28" lm="8" met="8"><w n="28.1">Qu</w>'<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="2">e</seg>c</w> <w n="28.3" punct="pt:3">v<seg phoneme="u" type="vs" value="1" rule="424" place="3" punct="pt ti">ou</seg>s</w>. — <w n="28.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">On</seg></w> <w n="28.5" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="13" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Octobre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>