<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ44" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abba] 4[abab] 2[aa]">
					<head type="number">XLIV</head>
					<head type="main">LA CANNE</head>
					<lg n="1" type="regexp" rhyme="abbaababababababaaababaa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="408" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>s</w> ! <w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">ai</seg>t</w> <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="7">ain</seg>c<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">M<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>ltk<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="408" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="2.4" punct="pv:5">m<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" punct="pv">o</seg>rt</w> ; <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>l</w> <w n="2.6" punct="pe:8">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>sp<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></w> !…</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="442" place="1" punct="vg">O</seg>r</w>, <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg></w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">qu</w>'<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>l</w> <w n="3.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="372" place="6">en</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8" punct="dp:8">d<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="4" num="1.4" lm="8" met="8">« <w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="2">un</seg></w> <w n="4.3" punct="vg:4">f<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>l</w>, <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="4.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="408" place="7">é</seg>c<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="449" place="8" punct="vg">u</seg></rhyme></w>,</l>
						<l n="5" num="1.5" lm="8" met="8">» <w n="5.1" punct="vg:1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="1" punct="vg">en</seg></w>, <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>s</w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="452" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>c<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8">» <w n="6.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pv:8">r<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg></rhyme></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8">» <w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>s</w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">on</seg>s</w> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="452" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.5">c<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8">» <w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg>t</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg></rhyme></w> ! »</l>
						<l n="9" num="1.9" lm="8" met="8">— <w n="9.1"><seg phoneme="y" type="vs" value="1" rule="452" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="pe:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="9.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="6">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>d<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1" punct="pe:3">M<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="339" place="3" punct="pe">a</seg>l</w> ! <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w> <w n="10.3">n</w>'<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="496" place="5">y</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w> <w n="10.6" punct="pe:8">p<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</rhyme></w> !</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">p<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="11.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="11.5">c<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>d<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>s</w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="pe:8">p<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pe">a</seg>s</rhyme></w> !</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="490" place="2">i</seg></w> <w n="13.3">v<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>t</w> <w n="13.4">s</w>'<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="4">en</seg></w> <w n="13.6">d<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="6">er</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="13.8">p<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg>t</w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>s</w> <w n="14.5">d</w>'<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="6">un</seg></w> <w n="14.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>pl<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="pv">oi</seg></rhyme></w> ;</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="15.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">r<seg phoneme="e" type="vs" value="1" rule="408" place="5">é</seg>p<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>c<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="304" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>l</w> <w n="16.2">n</w>'<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="2">en</seg></w> <w n="16.4">f<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>t</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="16.6" punct="vg:5">pl<seg phoneme="y" type="vs" value="1" rule="449" place="5" punct="vg">u</seg>s</w>, <w n="16.7">cr<seg phoneme="wa" type="vs" value="1" rule="439" place="6">o</seg>y<seg phoneme="e" type="vs" value="1" rule="346" place="7">ez</seg></w>-<w n="16.8" punct="vg:8">m<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="422" place="8" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="17" num="1.17" lm="8" met="8"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1">ou</seg>r</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="466" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>str<seg phoneme="e" type="vs" value="1" rule="346" place="5">er</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="6">à</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="17.5" punct="vg:8">Pr<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="449" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="1.18" lm="8" met="8"><w n="18.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1">an</seg>s</w> <w n="18.2"><seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="18.3" punct="tc:7">p<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="418" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg ti">e</seg></w>, — <w n="18.4">f<rhyme label="a" id="9" gender="f" type="e" stanza="5" part="I"><seg phoneme="y" type="vs" value="1" rule="444" place="8">û</seg>t</rhyme></w>-<w n="18.5"><rhyme label="a" id="9" gender="f" type="e" stanza="5" part="F">c<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9">e</seg></rhyme></w></l>
						<l n="19" num="1.19" lm="8" met="8"><w n="19.1">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="1">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="4">e</seg></w>-<w n="19.3" punct="vg:8">V<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="10" gender="m" type="a" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="20" num="1.20" lm="8" met="8"><w n="20.1">S<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="20.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="419" place="4" punct="vg">oi</seg>s</w>, <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="20.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="20.7" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>l<rhyme label="b" id="11" gender="f" type="a" stanza="6"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="21" num="1.21" lm="8" met="8"><w n="21.1" punct="vg:2">B<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>rk</w>, <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="21.3">v<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>s</w>-<w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="7">e</seg>c</w> <w n="21.6" punct="tc:8"><rhyme label="a" id="10" gender="m" type="e" stanza="6"><seg phoneme="ø" type="vs" value="1" rule="397" place="8" punct="vg ti">eu</seg>x</rhyme></w>, —</l>
						<l n="22" num="1.22" lm="8" met="8"><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="22.2">pl<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>s</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="352" place="3">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="439" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.4">v<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>l<rhyme label="b" id="11" gender="f" type="e" stanza="6"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="23" num="1.23" lm="8" met="8"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="490" place="1">i</seg></w> <w n="23.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>s</w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="4">ai</seg>t</w> <w n="23.4">fr<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="408" place="6">é</seg></w> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.6">d<rhyme label="a" id="12" gender="m" type="a" stanza="7"><seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>s</rhyme></w></l>
						<l n="24" num="1.24" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="24.3">v<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="a" type="vs" value="1" rule="306" place="5">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7">e</seg>s</w> <w n="24.6" punct="pe:8">s<rhyme label="a" id="12" gender="m" type="e" stanza="7"><seg phoneme="o" type="vs" value="1" rule="437" place="8" punct="pe">o</seg>ts</rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Novembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>