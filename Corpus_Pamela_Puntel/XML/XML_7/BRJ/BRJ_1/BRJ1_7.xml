<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRJ1" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcddc)">
				<head type="main">A MON AMI E. COTTINET</head>
				<lg n="1" type="huitain" rhyme="ababcddc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="1.2">t<seg phoneme="wa" type="vs" value="1" rule="422" place="2">oi</seg></w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="490" place="3">i</seg></w> <w n="1.4">n</w>'<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>s</w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>s</w> <w n="1.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>mn<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="2.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="381" place="2">e</seg>il</w> <w n="2.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg></w>, <w n="2.4" punct="vg:5">qu<seg phoneme="i" type="vs" value="1" rule="490" place="5" punct="vg">i</seg></w>, <w n="2.5" punct="vg:6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>l</w>, <w n="2.6">p<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>t</w>-<w n="2.7" punct="vg:8"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">f<seg phoneme="y" type="vs" value="1" rule="444" place="7">û</seg>t</w> <w n="3.6" punct="vg:8">n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="408" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="307" place="2">ai</seg>s</w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>r</w> <w n="4.6" punct="vg:8">n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg></w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="381" place="4">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">m<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>ti<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="6.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>s<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="5">e</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="6.7" punct="pv:8">v<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="7.3">h<seg phoneme="ɛ" type="vs" value="1" rule="304" place="3">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">où</seg></w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>v<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">C<seg phoneme="i" type="vs" value="1" rule="466" place="1">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="2">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="4">o</seg>r</w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ti<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
					<signed>P. J. Barbier.</signed>
				</closer>
			</div></body></text></TEI>