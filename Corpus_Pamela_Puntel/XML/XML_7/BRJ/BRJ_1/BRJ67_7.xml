<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ67" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite périodique" schema="6(abab)">
					<head type="number">LXVII</head>
					<head type="main">LA SAINTE-VEHME</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>'<w n="1.2" punct="pi:4"><seg phoneme="a" type="vs" value="1" rule="339" place="1" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="2" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="466" place="3" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="pi ps">a</seg>t</w> ?… <w n="1.3" punct="pe:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pe ti" caesura="1">ai</seg>s</w> !<caesura></caesura> — <w n="1.4">C<seg phoneme="ɔ" type="vs" value="1" rule="418" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="1.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="301" place="10">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fm">e</seg></w>-<w n="1.7" punct="vg:12">V<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="12">e</seg>hm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="2.2">tr<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>b<seg phoneme="y" type="vs" value="1" rule="452" place="3" mp="M">u</seg>n<seg phoneme="o" type="vs" value="1" rule="317" place="4">au</seg>x</w> <w n="2.3" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="189" place="6" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>t</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="10" mp="C">e</seg>s</w> <w n="2.6" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>spr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="12" punct="vg">i</seg>ts</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="449" place="1" mp="P">u</seg>r</w> <w n="3.2">Gu<seg phoneme="i" type="vs" value="1" rule="484" place="2" mp="M">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="3.4">B<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>rk</w><caesura></caesura> <w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7">on</seg>t</w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="408" place="9">é</seg></w> <w n="3.7">l</w>'<w n="3.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>th<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>s</w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">à</seg></w> <w n="4.6" punct="pt:6">pr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="pt">i</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" punct="pe ps">on</seg></w> !… <w n="5.2">L</w>'<w n="5.3">h<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>r</w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4" mp="P">an</seg>s</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg>ill<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="7">e</seg>st</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="P">à</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="C">a</seg></w> <w n="5.9">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>lti<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">qu</w>'<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="2">e</seg>st</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="C">a</seg></w> <w n="6.5">n<seg phoneme="ɛ" type="vs" value="1" rule="383" place="4">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="6.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="357" place="6" caesura="1">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="P">à</seg></w> <w n="6.8">d</w>'<w n="6.9"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="8" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="343" place="9" mp="M">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="397" place="10">eu</seg>x</w> <w n="6.10" punct="pv:12">s<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>mm<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="189" place="12" punct="pv">e</seg>ts</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">on</seg></w> <w n="7.2" punct="vg:2">br<seg phoneme="a" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>s</w>, <w n="7.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>d</w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="C">i</seg>l</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6" punct="vg:6">f<seg phoneme="o" type="vs" value="1" rule="317" place="6" punct="vg" caesura="1">au</seg>t</w>,<caesura></caesura> <w n="7.7">t<seg phoneme="y" type="vs" value="1" rule="456" place="7">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="8">en</seg></w> <w n="7.9">pl<seg phoneme="ɛ" type="vs" value="1" rule="384" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.10" punct="vg:12">l<seg phoneme="y" type="vs" value="1" rule="452" place="11" mp="M">u</seg>mi<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>s</w> <w n="8.3">l</w>'<w n="8.4" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="8.5" punct="pe:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pe">ai</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu</w>'<w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="464" place="1" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="9.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5" mp="M">o</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" caesura="1">ai</seg>ts</w><caesura></caesura> <w n="9.5">d</w>'<w n="9.6"><seg phoneme="y" type="vs" value="1" rule="452" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">r<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="169" place="10" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">F<seg phoneme="o" type="vs" value="1" rule="317" place="1" mp="Lp">au</seg>t</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="P">a</seg>r</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">p<seg phoneme="wa" type="vs" value="1" rule="419" place="5" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.6">p<seg phoneme="y" type="vs" value="1" rule="452" place="7" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>r</w> <w n="10.7">l</w>'<w n="10.8" punct="pi:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="9" mp="M">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="419" place="10" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="443" place="11" mp="M">o</seg>nn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="pi">eu</seg>r</rhyme></w> ?</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1" mp="C">On</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="y" type="vs" value="1" rule="449" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="339" place="7" mp="C">a</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="P">à</seg></w> <w n="11.8">l</w>'<w n="11.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="481" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="12.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.5" punct="pe:6">h<seg phoneme="o" type="vs" value="1" rule="443" place="5">o</seg>nn<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe">eu</seg>r</rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">N</w>'<w n="13.2"><seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>s</w> <w n="13.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="339" place="4" punct="vg">a</seg>s</w>, <w n="13.4" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="449" place="5" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="6" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">l</w>'<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="9">en</seg>t</w> <w n="13.8">pr<seg phoneme="ɔ" type="vs" value="1" rule="438" place="10" mp="M">o</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>t<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">qu</w>'<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>l</w> <w n="14.4" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="338" place="3" punct="vg">a</seg>y<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="14.6">fl<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="442" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9">un</seg></w> <w n="14.9">t<seg phoneme="ɛ" type="vs" value="1" rule="345" place="10">e</seg>l</w> <w n="14.10" punct="pv:12">d<seg phoneme="ɛ" type="vs" value="1" rule="357" place="11" mp="M">e</seg>ss<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="385" place="12" punct="pv">ein</seg></rhyme></w> ;</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="346" place="3">er</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4" mp="C">a</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="15.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="C">on</seg></w> <w n="15.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="408" place="9" mp="M">é</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="15.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="11" mp="C">on</seg></w> <w n="15.9" punct="pe:12">t<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="456" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
						<l n="16" num="4.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="16.1">L</w>'<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="211" place="2">en</seg>t</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t</w> <w n="16.4">l</w>'<w n="16.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>ss<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">Qu</w>'<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="17.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="2" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>t</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="4" mp="C">e</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">a</seg>j<seg phoneme="y" type="vs" value="1" rule="449" place="6" caesura="1">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="8" mp="C">e</seg>s</w> <w n="17.8">fr<seg phoneme="a" type="vs" value="1" rule="339" place="9">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="17.10">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="17.11" punct="vg:12">t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="1" mp="P">an</seg>s</w> <w n="18.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="357" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="4">ou</seg>p</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="18.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg></w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="339" place="9" mp="P">a</seg>r</w> <w n="18.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="18.9" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>s<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="12" punct="vg">a</seg>rd</rhyme></w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">j<seg phoneme="u" type="vs" value="1" rule="424" place="3" mp="Lp">ou</seg>r</w>-<w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">à</seg></w> <w n="19.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="339" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="19.6">p<seg phoneme="u" type="vs" value="1" rule="424" place="7" mp="P">ou</seg>r</w> <w n="19.7">n<seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>s</w> <w n="19.8"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="9" mp="C">un</seg></w> <w n="19.9">j<seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>r</w> <w n="19.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="19.11" punct="pv:12">f<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="20" num="5.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">à</seg></w> <w n="20.3">b<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>s</w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5" punct="pe:6">p<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>gn<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe">a</seg>rd</rhyme></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="21.2">pr<seg phoneme="o" type="vs" value="1" rule="443" place="2" mp="M">o</seg>c<seg phoneme="e" type="vs" value="1" rule="408" place="3" mp="M">é</seg>d<seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>s</w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M">â</seg>ch<seg phoneme="ø" type="vs" value="1" rule="397" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="21.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="7" mp="M">on</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="365" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="P">à</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="C">a</seg></w> <w n="21.7" punct="pv:12">Pr<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="22" num="6.2" lm="12" met="6+6"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="C">I</seg>ls</w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="427" place="2" mp="M">ou</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="22.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="7">en</seg></w> <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="496" place="8" mp="C">y</seg></w> <w n="22.7">tr<seg phoneme="u" type="vs" value="1" rule="424" place="9" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>t</w> <w n="22.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="339" place="11" mp="M">a</seg>cc<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="12" punct="pt">è</seg>s</rhyme></w>.</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="424" place="1" mp="P">ou</seg>r</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="496" place="2" mp="C">y</seg></w> <w n="23.3">d<seg phoneme="o" type="vs" value="1" rule="443" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="346" place="4">er</seg></w> <w n="23.4">m<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="23.5" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6" punct="vg" caesura="1">oi</seg>x</w>,<caesura></caesura> <w n="23.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>l</w> <w n="23.7">f<seg phoneme="o" type="vs" value="1" rule="317" place="8" mp="M">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="307" place="9">ai</seg>t</w> <w n="23.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="23.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="23.10">f<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="449" place="12">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="24" num="6.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="24.1">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="1">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="2">en</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="24.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="24.4" punct="pe:6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>ç<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="6" punct="pe">ai</seg>s</rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">Décembre 1870.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>