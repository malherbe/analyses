<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">LE FRANC-TIREUR</title>
				<title type="medium">Édition électronique</title>
				<author key="BRJ">
					<name>
						<forename>Jules</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1825" to="1901">1825-1901</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3907 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">BRJ_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
						<author>Jules Barbier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Google Books</publisher>
						<idno type="URI">https://books.google.fr/books/about/Le_franc_tireur.html?id=0NEaAAAAYAAJ</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871</title>
								<author>Jules Barbier</author>
								<imprint>
									<pubPlace>Limoges</pubPlace>
									<publisher>CHEZ TOUS LES LIBRAIRES [Imp. Ve H. Ducourtieux]</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>LE FRANC-TIREUR, CHANTS DE GUERRE,  1870-1871 (DEUXIÈME ÉDITION)</title>
						<author>Jules Barbier</author>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>MICHEL LEVY, FRÈRES, ÉDITEURS</publisher>
							<date when="1871">1871</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-27" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-27" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE FRANC-TIREUR</head><div type="poem" key="BRJ19" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1(ababcdcd) 1(abba)">
				<head type="number">XIX</head>
					<head type="main">GUITARE</head>
					<div n="1" type="section">
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="1.2">Pr<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">n</w>'<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="4">e</seg>st</w> <w n="1.5">qu</w>'<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="5">un</seg></w> <w n="1.7" punct="pv:8">v<seg phoneme="o" type="vs" value="1" rule="443" place="6">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="pv">i</seg>f</rhyme></w> ;</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="2.2" punct="vg:3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="2.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>t</w> <w n="2.4">s<seg phoneme="wa" type="vs" value="1" rule="419" place="5">oi</seg>t</w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg></w> <w n="2.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="466" place="2">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>t</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4">on</seg></w> <w n="3.3">n<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rf</w> <w n="3.4" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="438" place="6">o</seg>lf<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>ct<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8" punct="vg">i</seg>f</rhyme></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="1">En</seg></w> <w n="4.2">b<seg phoneme="wa" type="vs" value="1" rule="419" place="2">oi</seg>t</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="452" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pv:8">r<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="317" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>t<seg phoneme="o" type="vs" value="1" rule="414" place="3" punct="vg">ô</seg>t</w>, <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="418" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.4" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="vg">on</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D</w>'<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="317" place="2">au</seg>t</w>-<w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w>-<w n="6.5">c<seg phoneme="œ" type="vs" value="1" rule="248" place="4">œu</seg>r</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>j<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="409" place="3">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="5">e</seg>c</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5" punct="pv:8">p<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>s<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8" punct="pv">on</seg></rhyme></w> ;</l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="188" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">in</seg></w> <w n="8.4">lu<seg phoneme="i" type="vs" value="1" rule="490" place="4">i</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="357" place="5">e</seg>rt</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7" punct="pe:8">c<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>v<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="357" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">Septembre 1870.</date>
							</dateline>
						</closer>
					</div>
					<div n="2" type="section">
						<lg n="1" type="quatrain" rhyme="abba">
							<l n="9" num="1.1" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">r<seg phoneme="a" type="vs" value="1" rule="339" place="3">a</seg>b<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>ts</w> <w n="9.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="376" place="5">en</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6" punct="pv:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="pv">a</seg></rhyme></w> ;</l>
							<l n="10" num="1.2" lm="8" met="8"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="10.2">c<seg phoneme="y" type="vs" value="1" rule="449" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="307" place="5">aî</seg>t</w> <w n="10.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="307" place="7">ai</seg>s<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="11" num="1.3" lm="8" met="8"><w n="11.1">C</w>'<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="1">e</seg>st</w> <w n="11.3" punct="pv:2">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" punct="pv">ai</seg></w> ; <w n="11.4">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="3">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg></w> <w n="11.7" punct="pv:8">n<seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>s<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="12" num="1.4" lm="8" met="8"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="307" place="1">ai</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg></w> <w n="12.3">c<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="357" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="5">e</seg>st</w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="424" place="7">ou</seg>rs</w> <w n="12.6" punct="pe:8">l<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8" punct="pe">à</seg></rhyme></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1870">Novembre 1870.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>