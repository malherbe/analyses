<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="corpus">corpus Pamela Puntel</title>
				<title type="main">AMERTUMES ET PAIN NOIR</title>
				<title type="sub">SIÈGE DE PARIS 1870-1871</title>
				<title type="medium">Édition électronique</title>
				<author key="FRS">
					<name>
						<forename>Émile</forename>
						<surname>FRANÇOIS</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation des textes</resp>
					<name id="PP">
						<forename>Pamela</forename>
						<surname>Puntel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>487 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
						<author>ÉMILE FRANÇOIS</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k61012844.r=FRAN%C3%87OIS%20E.%2C%20AMERTUMES%20ET%20PAIN%20NOIR.?rk=21459;2</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>AMERTUMES ET PAIN NOIR — SIÈGE DE PARIS 1870-1871</title>
								<author>ÉMILE FRANÇOIS</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>LIBRAIRIE INTERNATIONALE A. LACROIX, VERBOECKHOVEN ET Cie</publisher>
									<date when="1871">1871</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s'inscrit dans un projet de constitution d'un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires sont formatées grossièrement.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L'insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L'orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les traits d'union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les parties de texte en italiques n'ont pas été délimitées par la balise appropriée.</p>
					<p>Le découpage en strophes a été parfois corrigé.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-11-24" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2019-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).(</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRS2" modus="cp" lm_max="10" metProfile="8, 4+6" form="suite périodique" schema="1(ababbccb) 2(ababcdcd)">
				<head type="main">L'INVESTISSEMENT</head>
				<lg n="1" type="huitain" rhyme="ababbccb">
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">qu</w>'<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ls</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>t</w> <w n="1.5" punct="vg:4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="4" punct="vg">on</seg>gs</w>, <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="160" place="5">e</seg>s</w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>rs</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.9" punct="pe:8">si<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></w> !…</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1">e</seg>s</w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="2.3">qu</w>'<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="4">oi</seg>t</w> <w n="2.6">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>r</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg>pr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>s</rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg></w> <w n="3.2" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="408" place="2">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="307" place="3">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.4" punct="pe:8">c<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></w> !…</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="4.2">l</w>'<w n="4.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="169" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg></w>, <w n="4.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-1" place="5">e</seg>r</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="449" place="6">u</seg></w> <w n="4.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="449" place="7">u</seg>cc<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="8" punct="vg">è</seg>s</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>r</w> <w n="5.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="188" place="4">e</seg>t</w> <w n="5.4" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="vg">am</seg>ps</w>, <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="5.6" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="443" place="7">o</seg>r<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">ê</seg>ts</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">Qu</w>'<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>l</w> <w n="6.3">f<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="188" place="3">e</seg>t</w> <w n="6.5">p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="345" place="6">e</seg>c</w> <w n="6.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="317" place="7">au</seg>d<rhyme label="c" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="339" place="1">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.2">s</w>'<w n="7.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="408" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg">i</seg>t</w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">en</seg>l<rhyme label="c" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2">e</seg>s</w> <w n="8.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="3">on</seg>gs</w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>gs</w> <w n="8.5">v<seg phoneme="o" type="vs" value="1" rule="437" place="5">o</seg>s</w> <w n="8.6" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="449" place="6" punct="vg">u</seg>rs</w>, <w n="8.7" punct="pe:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>ç<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="307" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababcdcd">
					<l n="9" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="3">e</seg>s</w> <w n="9.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">am</seg>p<seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="7">a</seg></w> <w n="9.5" punct="vg:8">f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">ch<seg phoneme="a" type="vs" value="1" rule="339" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg></w> <w n="10.4">f<seg phoneme="ɔ" type="vs" value="1" rule="438" place="5">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="357" place="7">e</seg>rr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="11" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="339" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="11.3">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.4" punct="vg:7">p<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-27" place="7" punct="vg">e</seg></w>, <w n="11.5" punct="vg:8">h<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="12" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1">Fl<seg phoneme="o" type="vs" value="1" rule="437" place="1">o</seg>t</w> <w n="12.2" punct="vg:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>t</w>, <w n="12.3">fl<seg phoneme="o" type="vs" value="1" rule="437" place="4">o</seg>t</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="5">en</seg>v<seg phoneme="a" type="vs" value="1" rule="339" place="6">a</seg>h<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ss<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>r</rhyme></w></l>
					<l n="13" num="2.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">D</w>'<w n="13.2" punct="vg:2">h<seg phoneme="ɔ" type="vs" value="1" rule="418" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">ch<seg phoneme="o" type="vs" value="1" rule="443" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="188" place="6">e</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="ps:8">b<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</rhyme></w>…</l>
					<l n="14" num="2.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="438" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="424" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="14.5" punct="vg:8">p<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="vg">a</seg>rts</rhyme></w>,</l>
					<l n="15" num="2.7" lm="8" met="8"><space unit="char" quantity="4"></space><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">On</seg></w> <w n="15.2">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="317" place="3">au</seg>x</w> <w n="15.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>ds</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="339" place="5">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.6">l<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>rs</w> <w n="15.7" punct="vg:8">t<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="16" num="2.8" lm="8" met="8"><space unit="char" quantity="4"></space><w n="16.1">Qu</w>'<w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="1">on</seg></w> <w n="16.3">c<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="3">en</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="339" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="317" place="6">au</seg>x</w> <w n="16.7" punct="ps:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="7">em</seg>p<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="339" place="8" punct="ps">a</seg>rts</rhyme></w>…</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababcdcd">
					<l n="17" num="3.1" lm="10" met="4+6"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="424" place="2" mp="P">ou</seg>r</w> <w n="17.3">s<seg phoneme="ɔ" type="vs" value="1" rule="438" place="3" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="17.4">n<seg phoneme="y" type="vs" value="1" rule="449" place="5">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="17.5">p<seg phoneme="ɔ" type="vs" value="1" rule="438" place="7">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="17.7">s</w>'<w n="17.8" punct="pt:10"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="18" num="3.2" lm="10" met="4+6"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>t</w> <w n="18.3">s</w>'<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="339" place="3" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="173" place="5">en</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="6" mp="C">a</seg></w> <w n="18.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="18.8" punct="ps:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="ps">é</seg></rhyme></w>…</l>
					<l n="19" num="3.3" lm="10" met="4+6"><w n="19.1">Qu</w>'<w n="19.2"><seg phoneme="o" type="vs" value="1" rule="317" place="1">au</seg></w> <w n="19.3">li<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg></w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="19.5">bru<seg phoneme="i" type="vs" value="1" rule="490" place="4" caesura="1">i</seg>ts</w><caesura></caesura> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="19.7">r<seg phoneme="y" type="vs" value="1" rule="456" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.8"><seg phoneme="o" type="vs" value="1" rule="317" place="7" mp="C">au</seg></w> <w n="19.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="416" place="8">oin</seg></w> <w n="19.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="19.11">c<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="424" place="10">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="20" num="3.4" lm="10" met="4+6"><w n="20.1">D</w>'<w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="451" place="1">un</seg></w> <w n="20.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="2">on</seg>g</w> <w n="20.4" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="449" place="3" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="449" place="4" punct="vg" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="5" mp="M">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>x</w> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="188" place="7">e</seg>t</w> <w n="20.7" punct="ps:10">ch<seg phoneme="y" type="vs" value="1" rule="449" place="8" mp="M">u</seg>ch<seg phoneme="o" type="vs" value="1" rule="434" place="9" mp="M">o</seg>tt<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="408" place="10" punct="ps">é</seg></rhyme></w>…</l>
					<l n="21" num="3.5" lm="10" met="4+6"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>d</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="317" place="2" mp="C">au</seg>x</w> <w n="21.3">m<seg phoneme="y" type="vs" value="1" rule="449" place="3">u</seg>rs</w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="424" place="4" caesura="1">ou</seg>rds</w><caesura></caesura> <w n="21.5"><seg phoneme="y" type="vs" value="1" rule="452" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="21.6">v<seg phoneme="wa" type="vs" value="1" rule="419" place="7">oi</seg>x</w> <w n="21.7">s<seg phoneme="a" type="vs" value="1" rule="339" place="8" mp="M">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>l<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="22" num="3.6" lm="10" met="4+6"><w n="22.1">S<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="22.2">s</w>'<w n="22.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="4" punct="vg" caesura="1">en</seg>d</w>,<caesura></caesura> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="C">a</seg></w> <w n="22.5">v<seg phoneme="wa" type="vs" value="1" rule="419" place="6">oi</seg>x</w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="160" place="7" mp="C">e</seg>s</w> <w n="22.7">n<seg phoneme="wa" type="vs" value="1" rule="419" place="8">oi</seg>rs</w> <w n="22.8" punct="ps:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>n<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="ps">on</seg>s</rhyme></w>…</l>
					<l n="23" num="3.7" lm="10" met="4+6"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="443" place="1">O</seg></w> <w n="23.2">m<seg phoneme="ɛ" type="vs" value="1" rule="160" place="2" mp="C">e</seg>s</w> <w n="23.3" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="363" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe ps" caesura="1">an</seg>ts</w> !<caesura></caesura>… <w n="23.4">s<seg phoneme="a" type="vs" value="1" rule="339" place="5" mp="M/mp">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="346" place="6" mp="Lp">ez</seg></w>-<w n="23.5" punct="ps:7">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="7" punct="ps">e</seg></w>… <w n="23.6">c</w>'<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="198" place="8">e</seg>st</w> <w n="23.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="23.9" punct="pe:10">si<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="409" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
					<l n="24" num="3.8" lm="10" met="4+6"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="160" place="1" mp="C">e</seg>s</w> <w n="24.2">j<seg phoneme="u" type="vs" value="1" rule="424" place="2">ou</seg>rs</w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="24.4" punct="ps:4">si<seg phoneme="ɛ" type="vs" value="1" rule="409" place="4" punct="ps" caesura="1">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>…<caesura></caesura> <w n="24.5" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="408" place="5" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="339" place="6" punct="pe ps">a</seg>s</w> !… <w n="24.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="C">i</seg>ls</w> <w n="24.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="8">on</seg>t</w> <w n="24.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="9">en</seg></w> <w n="24.9" punct="pe:10">l<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="417" place="10" punct="pe ps">on</seg>gs</rhyme></w> !…</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">26 novembre.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>