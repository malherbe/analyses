<p align="center">
<img src="Images/Projet/corpus_malherbe_4.svg" alt="Corpus Malherbe" />
</p>

![CeCill](https://img.shields.io/badge/licence-CC_BY_NC_SA_3.0-red.svg?style=flat-square)
![CeCill](https://img.shields.io/badge/licence-CeCILL_2.1-red.svg?style=flat-square)
![CeCill](https://img.shields.io/badge/XML-1.0-blue.svg?style=flat-square)


## Relevés métriques de l'analyse du corpus _malherbə_

Les relevés métriques (tables au format csv) ont été générées automatiquement par les programmes d’analyse métrique du projet.

CRISCO (EA 4255) - Université de Caen Normandie

site du projet : [métrique en ligne](https://crisco2.unicaen.fr/verlaine/)

![ok|500x150](BDD/Graphiques/SVG/répartition_siècles.svg )


